const express = require('express');
const app = express();
const axios = require('axios');
const port = 4000;

app.use(express.json());


let books = [
    { id: 1, title: '1984', authorId: 1, ratingId: 1 },
    { id: 2, title: 'To Kill a Mockingbird', authorId: 2, ratingId: 2 },
    { id: 3, title: 'The Great Gatsby', authorId: 3, ratingId: 3 },

];

app.get('/books', async (req, res) => {
    const booksWithDetails = await Promise.all(books.map(async book => {
        const authorResponse = await axios.get(`http://api-autheur:5000/authors/${book.authorId}`);
        const ratingResponse = await axios.get(`http://api-avis:8000/ratings/${book.ratingId}`);

        return {
            ...book,
            author: authorResponse.data ? authorResponse.data.author : 'Unknown',
            rating: ratingResponse.data ? ratingResponse.data.rating : 'No rating',
        };
    }));

    res.json(booksWithDetails);
});

// http://localhost:4000/books
app.post('/books', (req, res) => {
    const newBook = {
        id: books.length + 1,
        title: req.body.title,
        authorId: req.body.authorId,
        ratingId: req.body.ratingId,
    };
    books.push(newBook);
    res.status(201).json(newBook);
});

// http://localhost:4000/books/1
app.put('/books/:id', (req, res) => {
    const id = parseInt(req.params.id);
    let updatedBook;
    books = books.map(book => {
        if(book.id === id) {
            updatedBook = { ...book, ...req.body };
            return updatedBook;
        }
        return book;
    });

    if (!updatedBook) {
        return res.status(404).json({ message: 'Book not found' });
    }

    res.json(updatedBook);
});

// http://localhost:4000/books/id

app.delete('/books/:id', (req, res) => {
    const id = parseInt(req.params.id);
    const bookIndex = books.findIndex(book => book.id === id);

    if (bookIndex === -1) {
        return res.status(404).json({ message: 'Book not found' });
    }

    books.splice(bookIndex, 1);
    res.status(204).end();
});

app.listen(port, () => {
    console.log(`API listen on http://localhost:${port}`);
});
