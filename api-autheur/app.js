const express = require('express');
const app = express();
const axios = require('axios');
const port = 5000;

app.use(express.json());


let authors = [
    { id: 1, author: 'George Orwell'},
    { id: 2, author: 'Harper Lee'},
    { id: 3, author: 'F. Scott Fitzgerald'},

];

// http://localhost:5000/authors/:id
app.get('/authors/:id', (req, res) => {
    const id = parseInt(req.params.id);
    const author = authors.find(author => author.id === id);

    if (!author) {
        return res.status(404).json({ message: 'Author not found' });
    }

    res.json(author);
});

// http://localhost:5000/authors
app.post('/authors', (req, res) => {
    const newAuthor = req.body;
    authors.push(newAuthor);
    res.status(201).json(newAuthor);
});

// http://localhost:5000/authors/1
app.put('/authors/:id', (req, res) => {
    const id = parseInt(req.params.id);
    let updatedAuthor;
    authors = authors.map(author => {
        if(author.id === id) {
            updatedAuthor = { ...author, ...req.body };
            return updatedAuthor;
        }
        return author;
    });
    res.json(updatedAuthor);
});

// http://localhost:5000/authors/id

app.delete('/authors/:id', (req, res) => {
    const id = parseInt(req.params.id);
    authors = authors.filter(author => author.id !== id);
    res.status(204).end();
});

app.listen(port, () => {
    console.log(`API listen on http://localhost:${port}`);
});
