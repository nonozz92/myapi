const express = require('express');
const app = express();
const axios = require('axios');
const port = 8000;

app.use(express.json());


let ratings = [
    { id: 1, rating: '9.1'},
    { id: 2, rating: '8.4'},
    { id: 3, rating: '7.3'},

];

// http://localhost:8000/ratings/:id
app.get('/ratings/:id', (req, res) => {
    const id = parseInt(req.params.id);
    const rating = ratings.find(rating => rating.id === id);

    if (!rating) {
        return res.status(404).json({ message: 'Rating not found' });
    }

    res.json(rating);
});

// http://localhost:8000/ratings
app.post('/ratings', (req, res) => {
    const newRating = req.body;
    ratings.push(newRating);
    res.status(201).json(newRating);
});

// http://localhost:8000/ratings/1
app.put('/ratings/:id', (req, res) => {
    const id = parseInt(req.params.id);
    let updatedRating;
    ratings = ratings.map(rating => {
        if(rating.id === id) {
            updatedRating = { ...rating, ...req.body };
            return updatedRating;
        }
        return rating;
    });
    res.json(updatedRating);
});

// http://localhost:8000/ratings/id

app.delete('/ratings/:id', (req, res) => {
    const id = parseInt(req.params.id);
    ratings = ratings.filter(rating => rating.id !== id);
    res.status(204).end();
});

app.listen(port, () => {
    console.log(`API listen on http://localhost:${port}`);
});
